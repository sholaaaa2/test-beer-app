import React, { useEffect, useState } from "react";
import ВeerCard from "./components/ВeerCard";
import useStore from "./store/zustandStore";
import BeerPage from "./components/BeerPage";

export default function BeerApp() {
  const [currentBeer, setCurrentBeer] = useState(null);
  const [startPos, setStartPos] = useState(0)

  const testBeers = useStore(state => state.getBeers(startPos));
  const getAllBeersApi = useStore(state => state.getAllBeersApi);
  const uploadBeers = useStore(state => state.uploadBeers);
  const getSize = useStore(state => state.getSize)
  const removeBeer = useStore(state => state.removeBeer);
  const selectedId = useStore(state => state.selectedBeersId);
  const removeSelected = useStore(state => state.removeSelectedId);
  const changePage = useStore(state => state.changePage);

  const findById = useStore(state => state.findById);

  useEffect(() => {
    getAllBeersApi()
  }, [])

  const handleDelete = () => {
    for (const id of selectedId) {
      removeBeer(id);
      removeSelected(id);
    }
    if (getSize() < 15) {
      changePage();
      uploadBeers();
    }
  }
  const handleShowPage = (id) => {
    const beer = findById(id);
    setCurrentBeer(beer)
  }
  const handleHidePage = () => {
    setCurrentBeer(null)
  }
  const handleScroll = (e) => {
    const el = e.target
    if (el.scrollTop < 100 && startPos !== 0) {
      setStartPos(startPos - 5 )
    }else if (el.scrollHeight - (el.scrollTop + window.innerHeight) < 100) {
      console.log(startPos + 5);
      if (startPos%10 === 5) {
        changePage();
        uploadBeers();
      }
      setStartPos(startPos + 5 )
      
    }

  }

  return (
    <div 
      className="BeerApp"
      onScroll={handleScroll}
    >
      {
        (selectedId.length > 0) &&
        <button
          className="deleteBtn"
          onClick={handleDelete}
        >Delete</button>
      }

      <div className="page_wrapper">
        <div>
          {currentBeer && <BeerPage beer={currentBeer} handleClick={handleHidePage} />}
        </div>
      </div>

      <div className="beer_container_scroll">
        {testBeers.map((beer, i) => <ВeerCard beer={beer} key={beer.id} handleClick={handleShowPage} />)}
      </div>
    </div>
  );
}
