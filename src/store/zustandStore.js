import { create } from 'zustand'

async function fetchApi(set, get, i) {
    const response = await fetch(`https://api.punkapi.com/v2/beers?page=${i}`);
    const data = await response.json()
    set({ beers: get().beers.concat(data.map(i => ({...i, selected: false}))) })
}


const useStore = create((set, get) => ({
    beers: [],
    selectedBeersId: [],
    currentPage: 1,
    maxBeers: 15,
    getAllBeersApi: () => fetchApi(set, get, 1),
    uploadBeers: () => fetchApi(set, get, get().currentPage),
    getSize: () => get().beers.length,
    getBeers: (start) => get().beers.slice(start, get().maxBeers + start),
    removeBeer: (id) => set({ beers: get().beers.filter(beer => beer.id !== id) }),
    changePage: () => set({ currentPage: get().currentPage + 1 }),
    addSelectedId: (id) => set({ selectedBeersId: [...get().selectedBeersId, id] }),
    removeSelectedId: (id) => set({ selectedBeersId: get().selectedBeersId.filter(i => i !== id) }),
    findById: (id) => get().beers.find(beer => beer.id === id),
    changeSelect: (id) => {const beer = get().beers.find(beer => beer.id === id); beer.selected = !beer.selected}
}))
export default useStore