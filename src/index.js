import React from 'react';
import ReactDOM from 'react-dom/client';
import BeerApp from './BeerApp';
import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BeerApp />
  </React.StrictMode>
);

