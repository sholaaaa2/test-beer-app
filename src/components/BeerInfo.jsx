import React from 'react'

export default function BeerInfo({ name, tagline, first_brewed, description, image_url, abv }) {
    return (
        <>
            <div>
                <img src={image_url} alt={name.toLowerCase()} />
                <h1>{name} <br /> {first_brewed}</h1>
                <h3>{tagline}</h3>
            </div>

            <div>
                <p>{description}</p>
                <span className='abv'>{abv}%</span>
            </div>
        </>
    )
}
