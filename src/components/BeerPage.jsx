import React from 'react'
import BeerInfo from './BeerInfo'

export default function BeerPage({ beer, handleClick }) {
    const { mash_temp, fermentation } = beer.method,
        { malt, hops, yeast } = beer.ingredients
    return (
        <>
            <button
                onClick={handleClick}
            >&times;</button>

            <div className='info_container'>
                <BeerInfo {...beer} />
            </div>
            <div className='info_container'>
                <div className='general_info'>
                    <h1>General information about beer</h1>
                    <p>
                        pH: {beer.ph}
                    </p>
                    <p>
                        Volume: {beer.volume.value + ' ' + beer.volume.unit}
                    </p>
                    <p>
                        Boil Volume: {beer.boil_volume.value + ' ' + beer.boil_volume.unit}
                    </p>
                </div>

                <div className='info_wrapper'>
                    <h1>The method of brewing beer.</h1>
                    <h2>The delay temperature:</h2>
                    <p>
                        {`${mash_temp[0].temp.value} ${mash_temp[0].temp.unit}, duration ${mash_temp[0].duration ?? 0}m`}
                    </p>

                    <h2>The boiling temperature:</h2>
                    <p>
                        {`${fermentation.temp.value} ${fermentation.temp.unit}`}
                    </p>
                </div>

                <div className='info_wrapper'>
                    <h1>Ingridients:</h1>
                    <h2>Malt</h2>
                    <ul>
                        {malt.map((item, i) => {
                            return <li key={i}>
                                {`${item.name} - ${item.amount.value} ${item.amount.unit}`}
                            </li>
                        })}
                    </ul>
                    <h2>Hops</h2>
                    <ul>
                        {hops.map((item, i) => {
                            return <li key={i}>
                                {`${item.name} - ${item.amount.value} ${item.amount.unit}`}<br />
                                {`The moment of adding hops -  ${item.add}`}
                            </li>
                        })}
                    </ul>

                    <h2>Yeast</h2>
                    <ul>
                        <li>{yeast}</li>
                    </ul>
                </div>

                <div className='info_wrapper'>
                    <h1>Food Pairing</h1>
                    <ul>
                        {beer.food_pairing.map((item, i) => <li key={i}>{item}</li>)}
                    </ul>
                </div>
            </div>
        </>
    )
}
