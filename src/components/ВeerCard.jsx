import React, { useState } from 'react';
import useStore from '../store/zustandStore';
import BeerInfo from "./BeerInfo";

export default function ВeerCard({beer, handleClick}) {
    const addSelected = useStore(state => state.addSelectedId);
    const removeSelected = useStore(state => state.removeSelectedId);
    const changeSelect = useStore(state => state.changeSelect);
    
    const handleSelect = (id) => {
        if (!beer.selected) {
            addSelected(id)
        } else{
            removeSelected(id)
        }
        changeSelect(id)
    }

    return (
        <div
            id={beer.id}
            className={'beerCard' + (beer.selected ? " selected" : "")} 
            onClick={() => handleClick(beer.id)}
            onContextMenu={(e) => {e.preventDefault(); handleSelect(beer.id)}}
        >
            <BeerInfo {...beer} />
        </div>
    )
}
